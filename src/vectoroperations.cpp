void separationVector3D(double *particleOneCoord, double *particleTwoCoord, double sepVect[]) {
  sepVect[0] = particleOneCoord[0] - particleTwoCoord[0];
  sepVect[1] = particleOneCoord[1] - particleTwoCoord[1];
  sepVect[2] = particleOneCoord[2] - particleTwoCoord[2];
}

double vectorMagnitude3D(double *vector) {
  double mag = 0;
  for (int i = 0 ; i < 3 ; i++) {
    mag += vector[i] * vector[i];
  }
  return sqrt(mag);
}

void unitVector3D(double *vector, double unitVect[]) {
  double mag = vectorMagnitude3D(vector);
  for (int i = 0 ; i < 3 ; i++) {
    unitVect[i] = vector[i] / mag;
  }
}
