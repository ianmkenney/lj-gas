#ifndef POTENTIALS_H
#define POTENTIALS_H

#include <cmath>

class LennardJones {
public:
  static double potential(double distance, double sigma, double epsilon) {
    return (4*epsilon) * ( pow(sigma/distance,12) - pow(sigma/distance,6) );
  }

  static double force(double distance, double sigma, double epsilon) {
    return -(4*epsilon) * ( -12*pow(distance,-13)*pow(sigma,12) + 6 * pow(distance,-7)*pow(sigma,6));
  }
};

#endif
