#include <iostream>
#include "simulation.cpp"

int main()
{
  const int N = 250;
  ArgonSimulation<N> sim = ArgonSimulation<N>(1.45, 1e-8, 1e-12);
  sim.setPositions("uniform");
  sim.run();
  return 0;
}
