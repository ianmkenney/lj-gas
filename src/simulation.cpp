#include <cmath>
#include "potentials.cpp"
#include "vectoroperations.cpp"

template < int NPART >
class ArgonSimulation {
public:
	ArgonSimulation(double density, double totalTime, double dt) {
		this->density = density;
		this->particleVolume = this->mass / density ;
		this->boxLength = std::pow(N * this->particleVolume , 1/3.);
		this->totalTime = totalTime;
		this->dt = dt;
	}

	int setPositions(char *type) {
		if (type == "uniform") {
			std::cout << "Distributing particles in a uniform distribution" << std::endl;
			this->setUniformDistribution();
			return 0;
		}
		else {
			return 1;
		}
  }
  
  void run() {

	  for (int i = 0 ; i < this->N ; i++)
	  {
		  for (int j = 0 ; j < 3 ; j++ ) this->velocities[i][j] = 0.0;
	  }

    std::cout << "Starting Argon simulation. Simulation will run for " << this->totalTime << " seconds with a timestep of " << dt << " seconds. ( " << this->totalTime/this->dt << " steps )\n";
    std::cout << "   Box length:\t" << boxLength << " m" << std::endl;
    std::cout << "   Density:\t"    << density << " kg/m^3"<<std::endl;
    std::cout << "========= PROGRESS ==========\n";
    double currentTime = 0;
    double forces[this->N][3];
    int steps = 0;

    for (int i = 0 ; i < this->N ; i++)
    {
	for (int k = 0 ; k < 3 ; k++) forces[i][k] = 0;
    }

    while (1) {
      if ( currentTime >= this->totalTime ) {
	std::cout << "Simulation complete in " << steps << " steps" <<std::endl;
	break;
      }

      if (steps % 1000 == 0) {
	progressBar(currentTime, totalTime);
      }
      for (int i = 0 ; i < (this->N-1 ) ; i++) {
	for (int j = i+1 ; j < this->N ; j++) {
	  	forcePair(i, j, forces);
	
	}
      }
      for (int i = 0 ; i < N ; i++) {
      	updateCoordinates(i, forces);
      }
      currentTime += this->dt;
      steps++;
    }
  }

private:
  int N = NPART;
  double density, boxLength, particleVolume, number1D;
  double positions[NPART][3];
  double velocities[NPART][3];
  double totalTime, dt;
  
  const double mass = 6.6335209e-26;
  const double sigma = 3.4e-10;
  const double epsilon = 1.65e-21;

  void progressBar(double fractional, double total) {
    int percent = (int) (100 * fractional/total);
    std::cout << "[";
    for (int i = 0 ; i < 20 ; i++) {
      if (percent > 0) {
	std::cout << "*";
      }
      else {
	std::cout << "-";
      }
      percent -= 5;
    }
    std::cout << "] (" << 100 + percent << "%) \n";
  }
  
  void forcePair(int i, int j, double forces[][3]) {
    double separationVector[3], unitVector[3];
    double separationMagnitude, forceComponent;
    
    separationVector3D(this->positions[j], this->positions[i], separationVector);
    unitVector3D(separationVector, unitVector);
    separationMagnitude = vectorMagnitude3D(separationVector);
    double forceMagnitude = LennardJones::force(separationMagnitude, sigma, epsilon);
    for (int k = 0 ; k < 3 ; k++) {
	    forceComponent = unitVector[k] * forceMagnitude; 
      forces[i][k] += forceComponent;
      forces[j][k] -= forceComponent;
    }
  }

  void updateCoordinates(int i, double forces[][3]) {
    double acceleration;
	for (int k = 0 ; k < 3 ; k++) {
		acceleration = forces[i][k] / this->mass;
		this->velocities[i][k] += acceleration * this->dt;
		this->positions[i][k] += this->velocities[i][k] * this->dt;
		forces[i][k] = 0;
    }
  }

  void showPositions() {
	for ( int i = 0 ; i < N ; i++) {
		std::cout << this->positions[i][0] << "   " << this->positions[i][1] << "   " <<this->positions[i][2] << std::endl;
	}
  }

  void setUniformDistribution() {
	  double spacing = pow(this->particleVolume, 1/3.);
	  double number1D = ceil(this->boxLength / spacing);
	  double x, y, z;
	  int counter = 0;

	  x = y = z = spacing / 2;
	  for (int i = 0 ; i < number1D ; i++) {
		  for (int j = 0 ; j < number1D ; j++){
			  for (int k = 0 ; k < number1D ; k++) {
				  if (counter >= this->N) break;
				  this->positions[counter][0] = x + i * spacing;
				  this->positions[counter][1] = y + j * spacing;
				  this->positions[counter][2] = z + k * spacing;
				  counter++;
			  }
		  }
	  }

  }
};
